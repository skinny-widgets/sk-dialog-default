# Skinny Widgets Dialog for Default Theme


dialog element

```
npm i sk-dialog sk-dialog-default --save
```

then add the following to your html

```html
<sk-config
    theme="default"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-default"
></sk-config>
<sk-button id="myButton" button-type="primary">Show Dialog</sk-button>
<sk-dialog id="myDialog" title="Hello">
    Contents
</sk-dialog>
<script type="module">
    import { SkButton } from '/node_modules/sk-button/sk-button.js';
    import { SkDialog } from '/node_modules/sk-dialog/sk-dialog.js';
    customElements.define('sk-button', SkButton);
    customElements.define('sk-dialog', SkDialog);

    myButton.addEventListener('click', (event) => {
        myDialog.open();
    });
    myDialog.onconfirm = function(event) {
        console.log('confirmed');
        myDialog.close();
    };
    myDialog.oncancel = function(event) {
        console.log('cancelled');
        myDialog.close();
    };
</script>
```

As most of sk-elements sk-dialog by default renders it's contents inside Shadow DOM. So in case you want access some its
data you will have to to extend SkDialog class, implement getters that will access internals and return values and register
class as new Custom Element. It is quite easy and gives a better solution for code structure;)


#### slots

**label** - dialog window title

#### attributes

**to-body** - sometimes browser says it doesn't support dialogs in shadow dom (certainly with polyfill), 
but you can force it remount to body, although beaware, styles will be remounted too. 
Remounting will flush all event bindings added with **.addEventListener()** on internals so you will have to bind, 
everything at least on dialog instance or with dynamic late binding in addition components must be able to work property
with rerendering, e.g. dump and restore state to externals or attributes.

**open** - identifies dialog state, you can control it (open or close) by adding or removing this attribute 

#### Extending dialog actions

Sk Dialog allows you to use your own action buttons that are binded to methods and events by **action** (for sk-button) 
or **data-action** (for button) attributes. You can provided needed set of buttons by overriding dialog footer template. 

```html
<sk-dialog id="myDialog" title="Some Title" type="confirm" to-body>
    myDialog
    <sk-input id="skInput" sk-required></sk-input>
    <sk-checkbox id="skCheckbox" sk-required></sk-checkbox>
    <sk-switch id="skSwitch" sk-required></sk-switch>
    <template id="SkDialogFooterTpl">
        <div>
            <sk-button action="cancel" type="button">Cancel</sk-button>
            <sk-button action="save" type="button" button-type="primary">Save</sk-button>
        </div>
    </template>

</sk-dialog>
<script type="module">
    import { SkDialog } from '/node-modules/sk-dialog/src/sk-dialog.js';
    customElements.define('sk-dialog', SkDialog);
    myDialog.addEventListener('confirm', function(event) {
        console.log('confirmed');
    });
    myDialog.addEventListener('yo', function(event) {
        console.log('yoyo');
    });
    myDialog.oncancel = function (event) {
        console.log('cancelled');
        this.close();
    };
</script>
``` 

Note: Template ids must be prefixed according to constructor/class name, e.g. in case you are doing dialog with,
extending SkDialog class your footer template fill be search by id "YourClassNameFooterTpl".

#### template

id: SkDialogTpl, SkDialogFooterTpl