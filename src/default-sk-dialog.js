


import { SkDialogImpl }  from '../../sk-dialog/src/impl/sk-dialog-impl.js';


export class DefaultSkDialog extends SkDialogImpl {

    get styles() {
        return ['default.css'];
    }

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'dialog';
    }

    bindEvents() {
        super.bindEvents();
        this.polyfillNativeDialog(this.dialog);
    }

    get btnCancelEl() {
        return this.dialog.querySelector('.btn-cancel');
    }

    get btnConfirmEl() {
        return this.dialog.querySelector('.btn-confirm');
    }

    get tplFooterPath() {
        if (! this._tplFooterPath) {
            this._tplFooterPath = (this.comp.configEl && this.comp.configEl.hasAttribute('tpl-path'))
                ? `/${this.comp.configEl.getAttribute('tpl-path')}/${this.prefix}-sk-${this.suffix}-footer.tpl.html`
                :`/node_modules/sk-${this.comp.cnSuffix}-${this.prefix}/src/${this.prefix}-sk-${this.suffix}-footer.tpl.html`;
        }
        return this._tplFooterPath;
    }

    get headerEl() {
        return this.dialog.querySelector('.header');
    }

    get dialog() {
        if (! this._dialog) {
            this._dialog = this.comp.el.querySelector(this.comp.dialogTn);
        }
        return this._dialog;
    }

    set dialog(dialog) {
        this._dialog = dialog;
    }

    get subEls() {
        return ['dialog'];
    }

    get body() {
        return this.dialog.querySelector('.contents');
    }

    get footerEl() {
        return this.dialog.querySelector('.footer');
    }

    beforeRendered() {
        super.beforeRendered();
        this.saveState();
    }

    afterRendered() {
        //super.afterRendered();
        if (this.dialog) {
            this.polyfillNativeDialog(this.dialog);
            this.dialog.querySelector('.contents').insertAdjacentHTML('beforeend', this.contentsState);
            this.renderTitle();
            if (this.comp.type === 'confirm') {
                this.renderFooter();
            }
            this.mountStyles();
        }

    }

    open() {
        this.dialog.showModal();
        if (! this.comp.hasAttribute('open')) {
            this.comp.setAttribute('open', '');
        }
        this.open = true;
    }

    close() {
        this.dialog.close();
        this.comp.removeAttribute('open');
        this.open = false;
    }

    renderTitle() {
        if (this.comp.title) {
            this.headerEl.style.display = 'block';
            this.headerEl.innerHTML = this.comp.title;
        } else {
            this.headerEl.style.display = 'none';
        }
    }
}
